<template lang="pug">

#app
  .columns.is-vcentered
     .interactive-bg.column.is-4
     .login.column.is-4
        section.section("v-if"="block === 'first'")
             .has-text-centered
                 img.login-logo(src="./assets/images/logo_r_resumme.png" )
             .field
                 label.label Correo
                 .control.has-icons-right
                      input.input(type="text","v-model"="frm.email")
                      span.icon.is-small.is-right
                          i.fa.fa-user
             .field
                label.label Contraseña
                .control.has-icons-right
                      input.input(type="password","v-model"="frm.pass1")
                      span.icon.is-small.is-right
                          i.fa.fa-key
             .field
                label.label Confirmar Contraseña
                .control.has-icons-right
                      input.input(type="password","v-model"="frm.pass2","@blur"="checkPass")
                      span.icon.is-small.is-right
                          i.fa.fa-key
             .has-text-centered
                a.button.is-vcentered.is-info.is-medium("@click"="signUp", :disabled="frm.disabled") Registrarse
             .notification.is-danger("v-show"="Err.Show") {{ Err.Msg }}

        section.section("v-if"="block === 'second'")
          .has-text-centered
              img.login-logo(src="./assets/images/logo_r_resumme.png" alt="Ubimed")
          .has-text-centered Se ha enviado un correo a:
              br
              br
              .notification {{ frm.email }}
              .field
                  label.label Numero de Confirmación:
                  .control.has-icons-right
                       input.input.is-medium(type="text",len=6,"v-model"="frm.code","@blur"="checkCode")
                       span.icon.is-small.is-right
                           i.fa.fa-user
              .has-text-centered
                  a.button.is-vcentered.is-medium.is-info("@click"="confCode(frm.code)", :disabled="frm.disabled") Confirmar

        section.section("v-if"="block === 'third'")
          .has-text-centered
              img.login-logo(src="./assets/images/logo_r_resumme.png" alt="Ubimed")
              .notification Tu correo ha sido confirmado.
              .has-text-centered
                  a.button.is-vcentered.is-medium.is-info(href="/login") Login

     .interactive-bg.column.is-4

</template>

<script>

const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
import { CognitoUserPool, CognitoUserAttribute, CognitoUser } from 'amazon-cognito-identity-js';

export default {
  name: 'app',
  data () {
    return {
      frm: {
        email: '',pass1: '',pass2: '', code: '',
        disabled: true
      },
      block: "first",
      Err: {
          Msg: "", Show: false
      },
      poolData : {
          UserPoolId : 'us-east-1_55555555', //  user pool 
          ClientId : '55555555555555555' //  app client id 
      }
    }
  },
  methods: {
    checkPass () {
      if(this.frm.pass1 === this.frm.pass2){
        this.frm.disabled = false;
      }
      else
      {
        this.frm.disabled = true;
      }
    },
    checkCode () {
      if(this.frm.code.length === 6)
      {
        this.frm.disabled = false;
      }
      else
      {
        this.frm.disabled = true;
      }
    },
    confCode: function(code){

      var that = this;
      var userPool = new AmazonCognitoIdentity.CognitoUserPool(this.poolData);
      var userData = {
          Username : this.frm.email,
          Pool : userPool
      };
      var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
      cognitoUser.confirmRegistration(code, true, function(err, result) {
          if (err) {
              alert(err.message || JSON.stringify(err));
              return;
          }
          console.log('call result: ' + result);
          that.block="third";
      });

    },
      
    signUp: function () {
          var that = this;
          const userPool = new AmazonCognitoIdentity.CognitoUserPool(this.poolData);
          const attributeList = [];
          userPool.signUp(this.frm.email, this.frm.pass1, attributeList, null, function(err, result){

             if(err){
                console.log(err);
                if(err.name == "UsernameExistsException")
                {
                  that.Err.Msg = "El usuario ya existe. Intenta con otro";
                  that.Err.Show = true;
                  return;
                }
                else if(err.name == "InvalidParameterException")
                {
                  that.Err.Msg = err.message;
                  that.Err.Show = true;
                  return;
                }
             }
                that.frm.disabled = true;
                that.block = "second";
                var cognitoUser = result.user;
                console.log('user name is ' + cognitoUser.getUsername());
          });
      }
    }
}
</script>

<style>

.interactive-bg {
  height: 100vh;
  background-color: #2892F4;
  -webkit-box-shadow: inset 24px 4px 64px -24px rgba(71,71,71,1);
  -moz-box-shadow: inset 24px 4px 64px -24px rgba(71,71,71,1);
  box-shadow: inset 24px 4px 64px -24px rgba(71,71,71,1);
  padding: 0px;
}

@media (max-width: 769px) {
  .interactive-bg{
    display: none
  }
}
.input {
  border-radius: 5px;
}
.button {
  margin-top: 20px;
  margin-bottom: 20px;
  min-width: 150px;
}
.login-logo {
  margin: 0 auto;
  margin-bottom: 50px;
  max-height: 100px;

}
.columns{
  margin: 0px;
}
</style>
