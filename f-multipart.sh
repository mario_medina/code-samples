#!/bin/bash
# Script developed by marioarz@
#

validation_failed () {
    echo 
    echo "Use: "
    echo "    $0 -size size[k|m] -bucket bucket_name [-folder folder_name] [-start part_number_start] [-end part_number_end] file"
    echo ""
    echo "    Options: "
    echo ""
    echo "    -size     Size of each fragment in which file will be splitted."
    echo "    -bucket   Name of the bucket where the file will be uploaded"
    echo "    -folder   (Optional) Path where the file will be uploded"
    echo "    -start    (Optional) First fragment number that will be uploaded as part of the current process"
    echo "    -end      (Optional) Last fragment number that will be uploaded as part of the current process"
    echo
    echo "Examples: "
    echo "           For 1GB File fragments, uploading all fragments:"
    echo "                 multi-part.sh -size 1024m -bucket myBucket myFile.mp4"
    echo ""
    echo "           For 500GB File fragments, starting in fragment 10 ending in fragment 30:"
    echo "                 multi-part.sh -size 512m -bucket myBucket -start 10 -end 30 myBucket myFile.mp4"
    echo ""
    echo "           For 10K File fragments, starting in fragment 100, finishing uploading the whole file"
    echo "                 multi-part.sh -size 10k -bucket myBucket -start 100 myFile.mp4"
    exit 1
}

numargs=$#
for ((i=1 ; i <= numargs ; i++))
do
    case "$1" in
      "-size")
        size=$2
        shift 2
        ;;
      "-bucket")
        bucket=$2
        shift 2
        ;;
      "-folder")
        folder=$2
        shift 2
        ;;
      "-start")
        inicio=$2
        shift 2
        ;;
      "-end")
        fin=$2
        shift 2
        ;;
      -*)
        echo "Unknown option"
        validation_failed
        ;;
    esac
done

file=$1

if [ -z "$bucket" ] || [ -z "$size" ] || [ -z "$1" ]  ; then
    validation_failed
fi

if [ -z "$folder" ]; then
   key=$file
else
   key="$folder/$file"
fi

echo "File: "$key
#Definiendo el inicio y el fin 
if [ ! -z "$inicio" ]; then
    if [[ $inicio -gt 1 ]] ; then
        aws s3 cp s3://$bucket/$key/mu-metadata/uploadid.txt .
        uploadId=$(cat uploadid.txt)
    fi
else
    inicio=1
fi

echo "Primer fragmento a subir: $inicio"
echo "* Haciendo split de archivo"

split -b $size $file tmp-multi-part-

last=$(ls -l tmp-multi-part-* | wc -l)
if [ -z "$fin" ]; then
    fin=$last
fi
echo "Ultimo fragmento a subir: $fin"

if [[ $inicio -eq 1 ]]; then
    echo "* Calculando MD5"
    md5file=$(openssl md5 -binary $file | base64)
    echo "* Creando multi-part"
    multiPart=$(aws s3api create-multipart-upload --bucket $bucket --key $key --metadata md5=$md5file)
    uploadId=$(echo $multiPart | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["UploadId"]')
    echo $uploadId > uploadid.txt
    aws s3 cp uploadid.txt s3://$bucket/$key/mu-metadata/
fi

echo "UploadId: $uploadId"
echo "* Subiendo archivo en partes"

i=1
for x in tmp-multi-part*; do 
  if [[ $i -ge $inicio ]] && [[ $i -le $fin ]]; then
      md5filepart=$(openssl md5 -binary $x | base64)
      part=$(aws s3api upload-part --bucket $bucket --key $key --part-number $i --body $x --upload-id $uploadId --content-md5 $md5filepart | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["ETag"]') 
      echo "$i. Archivo: $x | MD5: $md5filepart | ETag: $part"
      echo "{ \"ETag\": $part, \"PartNumber\":$i}," > etag-$i.txt
  fi
  rm $x
  i=$(($i + 1))
done

echo "Subiendo archivo a S3"
aws s3 sync . s3://$bucket/$key/mu-metadata/ --exclude "*" --include "etag-*"
rm etag-*
rm uploadid.txt

if [[ $fin -eq $last ]]; then

    echo "* Se ha subido el ultimo fragmento. Construyendo archivo..."
    
    aws s3 sync s3://$bucket/$key/mu-metadata/ . --quiet
    cat $(ls etag-* | sort -n -t - -k 2)> etags.final
    echo "{ \"Parts\": [" > fileparts-$file
    sed '$ s/.$//' etags.final >> fileparts-$file
    echo "]}" >> fileparts-$file

    aws s3 rm s3://$bucket/$key/mu-metadata --recursive --quiet
    aws s3api complete-multipart-upload --multipart-upload file://fileparts-$file --bucket $bucket --key $key --upload-id $uploadId

    rm etag* fileparts-$file uploadid.txt
    echo "Se creo el archivo!"
fi

echo "Finalizado"