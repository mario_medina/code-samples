'use strict';
const aws = require('aws-sdk');

// This is the handler that's invoked by Lambda
exports.handler = (event, context, callback) => {
    const ec2 = new aws.EC2();
    let instances = [];
    const param = {
        Filters: [{
            Name: "tag:autoStop",
            Values: ["on", "ON", "On"]
        },
        {
            Name: "instance-state-name",
            Values: ["running"]
        }]
    };

    // List instances that have the specified tags
    ec2.describeInstances(param, function (err, data) {
        if (err) {
            console.error(err.name);
            callback(err, null);
        } 
        else 
        {
          //We will receive the instances in an Reservations array initially. 
          //Each reservation may have also several instances
          if (data.Reservations.length > 0)
          {
              
            for (let i = 0; i < data.Reservations.length; i++) {
                for (let a = 0; a < data.Reservations[i].Instances.length; a++) {
                    
                    //Store the instanceid in 'instances' array
                    instances.push(data.Reservations[i].Instances[a].InstanceId);
                }
            }
              
            console.log("==== Stopping instances ==== ");
            console.log(instances);

            //Stop the instances
            ec2.stopInstances({
                InstanceIds: instances
            },
            function (err, data) {
                if (err) {
                    console.error(err.name);  
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });
          }
          else
          {
              console.log("No instances found with Tag");
              callback(null, data);
          }
        }
   });
};