/*
 * messages.c
 *
 *  Created on: 30/03/2014
 *      Author: Mario
 */

#include <string.h>
#include "bsp.h"
#include "mrfi.h"
#include "bsp_leds.h"
#include "bsp_buttons.h"
#include "nwk_types.h"
#include "nwk_api.h"
#include "nwk_frame.h"
#include "nwk.h"
#include "struct.h"
#include "definitions.h"
#include "messages.h"


uint8_t  Tid=0xFE;

uint8_t Send_byID(linkID_t lnkID, uint8_t type, uint8_t messg, uint8_t arg1, uint8_t ack)
{
    uint8_t msg[6];
    uint8_t misses, noAck;
    smplStatus_t rc;

    /* TiD */
    msg[0] = ++Tid;
    /* MSG Control */
    msg[1] = messg;
    /* Action*/
    msg[2] = type;
    msg[3] = arg1;

    if(ack==1)
    {
		for (misses=0; misses < MISSES_IN_A_ROW; ++misses)
		{
			  rc=SMPL_SendOpt(lnkID, msg, sizeof(msg), SMPL_TXOPTION_ACKREQ);
			  if (rc == SMPL_SUCCESS)
			  {
				return 1; // Se recibio ACK
			  }
			  if (rc == SMPL_NO_ACK )
			  {
				noAck++;
				NWK_DELAY(100);
			  }
		}
		if( noAck == MISSES_IN_A_ROW )
		{
			//No se recibo ACK
			return 0;
		}
    }
    else
    {
		SMPL_SendOpt(lnkID, msg, sizeof(msg), SMPL_TXOPTION_NONE);
		return lnkID;
    }
}

uint8_t Send_byGroup(uint16_t group, uint8_t type, uint8_t messg, uint8_t arg1){

	uint8_t msg[6];
    /* TYPE */
    msg[0] = ++Tid;
    /* MSG Control */
    msg[1] = messg;
    /* Group */
    msg[2] = group & 0xFF;
    msg[3] = (group >> 8) & 0xFF;
    /* Action*/
    msg[4] = type;
    msg[5] = arg1;

    SMPL_SendOpt(SMPL_LINKID_USER_UUD, msg, sizeof(msg), SMPL_TXOPTION_NONE);

	return 1;
}


void processMessage(linkID_t lid, uint8_t *msg, uint8_t len)
{
	uint8_t test = msg[6];

	switch(msg[1])
	{
		case MSG_INFO:
			register_new_device(msg,lid);
			break;
		case LED_TOGGLE:
			BSP_TOGGLE_LED1();
			break;
		case MEASURE_DATA:
			measure_data(msg,lid);
			break;
		case ACK_WITH_T:
			ack_with_temp(msg,lid);
			break;
	}
}

uint8_t isAlive(linkID_t lnkID){

	if (SMPL_SUCCESS != SMPL_Ping(lnkID))
	{
		return 0;
	}
	return 1;
}