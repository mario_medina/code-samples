const AWS = require('aws-sdk');
const ec2 = new AWS.EC2();
const ssm = new AWS.SSM();

exports.handler = (event, context, callback) => {

    console.log(event.Records[0].body);
    const message = JSON.parse(event.Records[0].body);
    let instanceId = '';
    let queue;
    
    if( message.jobid == null){
        callback("No JobId Received");
    }

    console.log("======= Attributes =======");
    console.log(event.Records[0].attributes);

    // Obtaining latest AMI-ID from Parameter Store:
    const ssmAmiId =  ssm.getParameter({ Name: 'ubimed.latest.ami' }).promise();

    ssmAmiId
      .then(function(data) {

        console.log("JobId: " + message.jobid);

        //Get Parameters
        const bucketName = process.env.BUCKET_NAME;
        const queueArn   = event.Records[0].eventSourceARN;
        queue = queueArn.split(':');

        // BWA
        let cmd = '';
        let options = {};
        options.amiId = data.Parameter.Value;

        switch(queue[5]) {
            case 'bwa-task-queue':
                cmd = "/home/ec2-user/bwa-scripts/process-bwa.sh " + message.jobid + " " + message.ref + " " +  message.r1 + " " +  message.r2 + " " +  bucketName + " & >> /var/log/ubimed.log";
                options.maxPrice= '0.70';
                options.instanceType= 'm4.10xlarge';
                options.queue= 'bwa';
                options.jobId = message.jobid;
                break;

            case 'dup-task-queue':
                cmd = "/home/ec2-user/bwa-scripts/process-dup.sh " + message.jobid + " " + message.ref + " " +  bucketName + " & >> /var/log/ubimed.log";
                options.maxPrice= '0.07';
                options.instanceType= 'm4.xlarge';
                options.queue= 'dup';
                options.jobId = message.jobid;
                break;

            default:
                const errorMsg="Queue not found";
                console.log(errorMsg);
                callback(errorMsg);
        }
        return runSpotInstance(cmd, options);
      })
      .then(function(data) {
        instanceId = data.Instances[0].InstanceId;

        if(queue[5]=="bwa-task-queue") {
          const dynamodb = new AWS.DynamoDB();
          const params = {
            Item: {
             "JobId":     { S: message.jobid },
             "Started":   { S: message.submitted },
             "CreatedBy": { S: message.user },
             "Comments":  { S: message.comments }
            },
            TableName: "ubimed"
           };
           return dynamodb.putItem(params).promise();
         }
         return;
      })
      .then(function() {
          console.log("InstanceId: " + instanceId );
          callback(null, 'Spot request launched on instance: ' + instanceId);
      })
      .catch(function(err) {
          console.log(err);
          callback(err);
      });
};

function runSpotInstance(cmd, options)
{
    // This Promise will place a Spot Instance request to run the specified instance.
    return new Promise(function(resolve, reject) {

      const userdata = '#!/bin/bash\n\
      mkdir /data\n\
      mkfs.ext4  /dev/xvdb\n\
      tune2fs -m 0 /dev/xvdb\n\
      mount /dev/xvdb /data\n\
      \n\
      chown ec2-user:ec2-user /data\n\
      \n\
      sed -i "s/JOBID/' + options.jobId + '/g" /etc/awslogs/awslogs.conf\n\
      service awslogs start\n\
      \n\
      export HOME=/root\n\
      export USER=root\n\
      cd $HOME\n\
      \n\
      sudo su - ec2-user --command="git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/bwa-scripts"\n\
      \n\
      cd /home/ec2-user/bwa-scripts\n\
      sudo chmod +x *.sh\n\
      sudo su - ec2-user --command="' + cmd + '"';

      const userb64 = new Buffer(userdata);
      const base64data = userb64.toString('base64');

      const params = {
        MaxCount: 1, MinCount: 1,
        ImageId: options.amiId,
        LaunchTemplate: {  LaunchTemplateName: 'UbimedTemplate' },
        UserData: base64data,
        InstanceMarketOptions: {
          MarketType: 'spot',
          SpotOptions: { MaxPrice: options.maxPrice }
        },
        InstanceType: options.instanceType
      };

      const ec2Run =  ec2.runInstances(params).promise();

      ec2Run
        .then(function(data) {
          return resolve(data);
        })
        .catch(function(err) {
          return reject(err);
        });
    });
}