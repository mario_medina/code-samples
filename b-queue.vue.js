<template lang="jade">
</template>

<script>
import AWS from 'aws-sdk';

export default {
  data() {
    return {
      queueURL: 'https://sqs.us-east-1.amazonaws.com/555555555555/bwa-task-queue',
    };
  },
  created() {
    console.log('** Queue');
    this.$parent.$on('send-to-bwa-queue', (jobInfo) => {
     
      //Validamos que la sesion este activa
      return window.awsSession()
        .then((session) => {
          
          const job = jobInfo;
          const timestamp = Math.round((new Date()).getTime() / 1000);
          
          job.user = window.user;
          job.submitted = timestamp.toString();
          
          if (session) {
            const sqs = new AWS.SQS();
            const params = {
              MessageBody: JSON.stringify(job),
              QueueUrl: this.queueURL,
              DelaySeconds: 0,
            };
            return sqs.sendMessage(params).promise();
          }
          else{
            throw new Error('Invalid User Session');
          }
        })
        .then((response) => {
          console.log('success');
          console.log(response);
          this.$parent.$emit('bwa-queue-success', 'ok');
        })
        .catch((err) => {
          console.log(err);
          this.$parent.$emit('bwa-queue-error', err);
        });
    });
  },
};
</script>
