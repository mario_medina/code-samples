/******************************************************************************************
 2014 - mario.medarz@gmail.com
*******************************************************************************************/
#include "bsp.h"
#include "mrfi.h"
#include "bsp_leds.h"
#include "bsp_buttons.h"
#include "nwk_types.h"
#include "nwk_api.h"
#include "nwk_frame.h"
#include "nwk.h"
#include "struct.h"
#include "definitions.h"
#include "nv_obj.h"
#include "messages_ed.h"
#include "type.h"
#include "adc.h"
#include "flash.h"
#include "stdlib.h" //for rand()

/*------------------------------------------------------------------------------
 * Globals
 *----------------------------------------------------------------------------*/
static volatile uint8_t sPeerFrameSem = 0;
static volatile uint8_t sBroadcastSem = 0;
ed	   myInfo;
uint8_t *Flash_Addr = SEG_A;

/*------------------------------------------------------------------------------
 * Callback Handler
 *----------------------------------------------------------------------------*/
static uint8_t sCB(linkID_t);

/*------------------------------------------------------------------------------
 * Prototypes
 *----------------------------------------------------------------------------*/
static void linkTo(void);
static void registration(void);
void createRandomAddress(void);
int TI_getRandomIntegerFromVLO(void);
void sendACK(void);

void main (void)
{
  void *nwk_cfg_flash;
  ioctlNVObj_t nwk_cfg_ram;
  uint8_t *data = NULL;
  addr_t lAddr;

  // Inicializamos Drivers
  BSP_Init();

  // leemos la direccion desde flash flash 
  lAddr.addr[0] = Flash_Addr[0];
  lAddr.addr[1] = Flash_Addr[1];
  lAddr.addr[2] = Flash_Addr[2];
  lAddr.addr[3] = Flash_Addr[3];

  // Establecer la direccion de red
  SMPL_Ioctl(IOCTL_OBJ_ADDR, IOCTL_ACT_SET, &lAddr);

  // Primero, validamos si tenemos algun contexto guardado
  nwk_cfg_flash = nv_obj_read_nwk_cfg();
  if(nwk_cfg_flash != NULL)
  {
	
	nwk_cfg_ram.objPtr = &data;
	uint8_t *grp_ptr = SEG_C;

	SMPL_Init(sCB);
	// restauramos la configuracion desde RAM
	if(SMPL_Ioctl(IOCTL_OBJ_NVOBJ, IOCTL_ACT_GET, &nwk_cfg_ram) == SMPL_SUCCESS)
	{
	  memcpy(data, nwk_cfg_flash, nwk_cfg_ram.objLen);
	}

    // Actualizamos nuestra estructura  
	myInfo.lnk	   = nv_obj_read_lnk_id(0);
    myInfo.model   = MODEL;
    myInfo.type    = TYPE;
    myInfo.sensors = SENSORS;
    myInfo.flags   = FLAGS;
    myInfo.group   = (grp_ptr[0]<<8) + grp_ptr[1];

	linkTo();
  }

  while (1)
  {

	  if(BSP_BUTTON1())
	  {
		  while(BSP_BUTTON1());
		  while (SMPL_SUCCESS != SMPL_Init(sCB))
		  {
			  BSP_TOGGLE_LED1();
			  NWK_DELAY(5);
		  }
		  while (SMPL_SUCCESS != SMPL_Link(&myInfo.lnk))
		  {
			BSP_TURN_ON_LED1();
		  }
		  //Salio de los dos while.. registrar equipo y mandar mi info
		  registration();
	  }
  }
}

static void registration(){

	ioctlNVObj_t nwk_cfg_ram;
	uint8_t *data = NULL;

	uint8_t msg[10];
    addr_t	myAddr;

    SMPL_Ioctl(IOCTL_OBJ_ADDR, IOCTL_ACT_GET, &myAddr);

    myInfo.group   = 0x0000;
    myInfo.model   = MODEL;
    myInfo.type    = TYPE;
    myInfo.sensors = SENSORS;
    myInfo.flags   = FLAGS;

	msg[0] = 0xFE;  			//Tid
	msg[1] = MSG_INFO;			//MSG
	msg[2] = myAddr.addr[0];
	msg[3] = myAddr.addr[1];
	msg[4] = myAddr.addr[2];
	msg[5] = myAddr.addr[3];
    msg[6] = myInfo.type;
    msg[7] = myInfo.model;
    msg[8] = myInfo.sensors;
    msg[9] = myInfo.flags;

    // TODO: Validar que se reciba a traves de ACK
	SMPL_SendOpt(myInfo.lnk, msg, sizeof(msg), SMPL_TXOPTION_NONE);

    nwk_cfg_ram.objPtr = &data;

    __disable_interrupt();
    
    // Guardar la configuracion de red en NVRAM
    if(SMPL_Ioctl(IOCTL_OBJ_NVOBJ, IOCTL_ACT_GET, & nwk_cfg_ram) == SMPL_SUCCESS)
    {
      if(nv_obj_write_nwk_cfg(data,nwk_cfg_ram.objLen) != true)
      {
        __no_operation(); // for debugging
      }
    }
    // guardamos linkId en NVRAM
    nv_obj_write_lnk_id(0, myInfo.lnk);
    __enable_interrupt();

	linkTo();
}

/*------------------------------------------------------------------------------
 * linkTo - En esta funcion, esperaremos leer por mensajes que recibamos
 * del AP 
 *----------------------------------------------------------------------------*/
static void linkTo()
{
  bspIState_t intState;
  ed_init();

  SMPL_Ioctl( IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_RXON, 0);

  while (1)
  {
		if (sPeerFrameSem)
		{
			uint8_t     msg[MAX_APP_PAYLOAD], len;
			if (SMPL_SUCCESS == SMPL_Receive(myInfo.lnk, msg, &len))
			{
			  processMessage(myInfo.lnk, msg, len);
			  BSP_ENTER_CRITICAL_SECTION(intState);
			  sPeerFrameSem--;
			  BSP_EXIT_CRITICAL_SECTION(intState);
			}
		}

		if(BSP_BUTTON1())
		{
			BSP_TURN_ON_LED1();
			while(BSP_BUTTON1());
			sendACK();
			BSP_TURN_OFF_LED1();
		}
  }
}

/* Runs in ISR context. Reading the frame should be done in the */
/* application thread not in the ISR thread. */
static uint8_t sCB(linkID_t lid)
{

  if (lid==myInfo.lnk)
    sPeerFrameSem++;
  else if(lid==SMPL_LINKID_USER_UUD)
    sBroadcastSem++;

  return 0;
}

void sendACK(){

	uint8_t msg[4];
	int temp;

	temp = getTemperature();

	msg[0] = 0xFE;
	msg[1] = ACK_WITH_T;
	msg[2] = temp & 0xFF;
	msg[3] = (temp>>8) & 0xFF;

	SMPL_SendOpt(myInfo.lnk, msg, sizeof(msg), SMPL_TXOPTION_NONE);
}



