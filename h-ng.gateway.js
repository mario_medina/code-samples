angular.module('ng.gateway', ['ui.mask'])

.config(['$stateProvider', '$urlRouterProvider', 'uiMask.ConfigProvider', function($stateProvider, $urlRouterProvider, uiMaskConf){
  
  uiMaskConf.maskDefinitions({'H': /[a-fA-F0-9]/});  
  
  $urlRouterProvider.when('/gateway', '/gateway/view');

  $stateProvider
    .state('gateway', {
      url: "/gateway",
      templateUrl: "/app/views/gateway/main",
      controller: 'GatewayCtrl'
    })
    .state('gateway.view', {
      url: "/view",
      templateUrl: "/app/views/gateway/view",
      controller: 'ViewGatewayCtrl'
    })
    .state('gateway.add', {
      url: "/add",
      templateUrl: "/app/views/gateway/add",
      controller: 'AddGatewayCtrl'
    })
}])

.controller('GatewayCtrl',[ '$scope', 'Gateway', '$window', function ($scope, Gateway, $window) {

    $scope.getGWs = function ()
    {
      $scope.loader   = true;
      Gateway.query().$promise
          .then(function(gws) {
              $scope.loader   = false;
              $scope.gateways = gws;
              $window.localStorage.gateways = JSON.stringify(gws);
          });
    }

    if(! $window.localStorage.gateways )
        $scope.getGWs();
    else{
        $scope.loader   = false;
        $scope.gateways = JSON.parse($window.localStorage.gateways);
    }
}])

.controller('ViewGatewayCtrl', [ '$scope', 'Gateway', '$window', '$uibModal', function ($scope, Gateway, $window, $uibModal) {

  var $ctrl = this;

  $ctrl.animationsEnabled = true;

  $scope.modalOpen = function (gateway) {
    
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'buscarLampara.html',      
      controllerAs: '$ctrl',
      controller: 'ModalInstanceCtrl',
      resolve: {
        gw: function () {
          return gateway;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
        $ctrl.selected = selectedItem;
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
    });
  }


}])

.controller('ModalInstanceCtrl', function ($uibModalInstance, $interval, gw) {
  
  var $ctrl = this;

  $ctrl.showBar = false;
  $ctrl.slider  = 0;

  $ctrl.ok = function () {
    $uibModalInstance.close($ctrl.selected.item);
  };

  $ctrl.search = function () {
    console.log("buscando lampara");
    $ctrl.showBar = true;

      var intevalSlider = $interval(function () { 
        $ctrl.slider  += 10;
        if($ctrl.slider > 100 ){
          $interval.cancel(intevalSlider);
          $ctrl.slider  = 0;
          $ctrl.showBar = false;
        }
      }, 2000);  
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})

.controller('AddGatewayCtrl', [ '$scope', 'Gateway', '$window', '$state', function ($scope, Gateway, $window, $state) {
   
    $scope.gw = {};
    $scope.inProgress = false;
    $scope.submitted  = false;

    $scope.errMsg = "";
    $scope.successMsg = "";

    $scope.addGW = function(isInvalid){

      $scope.submitted  = true;
      console.log(isInvalid);
      if(isInvalid) return;
      
      $scope.inProgress = true;
      $scope.errMsg = "";

      $scope.gw.name.replace(/ /g,"_");   //No se aceptan espacios. Los cambiamos
      var newGW = new Gateway($scope.gw);

      newGW.$save()
      .then(function (result) {
          $scope.getGWs();
          $state.transitionTo('gateway.view', { arg:'arg' });
      })
      .catch(function (err) {
          if     (err.status == 404) $scope.errMsg = "No se encontró el Gateway.";
          else if(err.status == 400) $scope.errMsg = "Verifica el # de Gateway";
          else  $scope.errMsg = "Error. Intenta nuevamente";
      })
      .finally(function(){
        $scope.inProgress = false;
      })
    }
}])

.filter('space', function() {
    return function(input) {
        var out;
        out = input.replace(/_/i, " ");
        return out;
    }
});