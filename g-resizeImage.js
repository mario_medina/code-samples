'use strict';

const AWS     = require('aws-sdk');
const im      = require('imagemagick');

// Update AWS configuration to set region
AWS.config.update({region : 'us-west-2'});

const s3 = new AWS.S3();

/**
 * Provide an event that contains the following keys:
 *
 *   - s3 object
 *   Images must reside in 'images/original/file.jpg' and will be saved
 *   into the 'images/<sizeStr>/file.jpg' path.
 */

function resizeImage(image, size, sizeStr, bucket, key){

  return new Promise(function(resolve, reject) {

    console.log("About to resize");
    
    //Usemos ImageMagik para dar nuevo tamano a la imagen
    im.resize({ srcData: image, width:  size, quality: 0.85}, function(err, stdout, stderr){
        if (err) {
          console.log("Error: " + JSON.stringify(err));
          reject(Error(err));
        }
        else
        {
            const cKey = key.replace(/original/g, sizeStr); 
            const s3putObject =  s3.putObject({ 
                Bucket: bucket, 
                Key: cKey, 
                Body: new Buffer(stdout, "binary"),
                ContentType: "image/jpeg"
            }).promise();      

            s3putObject
              .then(function(data) {
                  console.log('Uploaded resized image to: ' + cKey);
                  resolve('done');
              })
              .catch(function(err) {
                  console.log("Error: " + JSON.stringify(err));
                  reject(Error(err));
              })
        }
    });
  });
}

exports.handler = function(event, context) {
    
    //Necesitamos obtener primero el objeto.
    const srcBucket = event.Records[0].s3.bucket.name;
    const srcKey    = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " ")); 

    console.log("Bucket: " + srcBucket + "\tFile: " + srcKey);

    const s3getObject =  s3.getObject({ Bucket: srcBucket, Key: srcKey}).promise();

    s3getObject
      .then(function(data) {
          return Promise.all([
            resizeImage(data.Body, 137, 'thumbnail', srcBucket, srcKey),
            resizeImage(data.Body, 265, 'small',     srcBucket, srcKey), 
            resizeImage(data.Body, 356, 'medium',    srcBucket, srcKey),
            resizeImage(data.Body, 553, 'large',     srcBucket, srcKey),
            resizeImage(data.Body, 700, 'xlarge',    srcBucket, srcKey)
          ])
      })
      .then(function() {
        context.succeed('Success');
      })
      .catch(function(err) {
        console.log("Error: " + JSON.stringify(err));
        context.fail('Error');
      });
}//exports



