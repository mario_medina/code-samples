<template lang="jade">
.section.columns.is-8
  .column.is-2.has-text-centered
    span("v-for"="s in steps")
      .button.is-fullwidth.is-info.tooltip.is-tooltip-right(:data-tooltip="s.desc") {{ s.name }}
      i.fas.fa-arrow-down.fa-2x.fonta("v-show"="!s.last")
  .column.is-one-third
    dselect(:vals="ref","v-model"="frm.ref") Referencia
    dselect(:vals="r1","v-model"="frm.r1") R1
    dselect(:vals="r2","v-model"="frm.r2") R2
    .field
      label.label Comentarios del An�lisis
      .control
        textarea.textarea(placeholder='Textarea', "v-model"="frm.comments", ":disabled"="frm.disabled")
    .field
      .control
        label.label Prioridad
        label.radio
          input(type='radio', name='question', selected='selected')
          |       Normal
        label.radio
          input(type='radio', name='question')
          |       Alta
    .field
      .control
        .button.is-link.is-large("@click"="sendJob", ":disabled"="frm.disabled") Enviar Trabajo
  .column
    .tile.is-ancestor
      .tile.is-parent
        .tile.is-child.notification
          .title An�lisis Som�tico
          .content
           p | Lorem ipsum dolor sit amet, m
    .tile.is-ancestor("v-show"="job.sent")
      aws-queue
      .tile.is-parent
        .tile.is-child.card
          header.card-header
            p.card-header-title
              | Resumen del An�lisis
          .card-content
            .content
              .notification.is-info JobId: {{ job.id }}
              ul
              li Referencia: {{ frm.ref }}
              li R1:  {{ frm.r1 }}
              li R2:  {{ frm.r2 }}
          footer.card-footer
            a.card-footer-item(href='#') Ver detalles
    .tile.is-ancestor("v-show"="job.error")
      .tile.is-parent
        .tile.is-child.notification.is-danger
          .subtitle error
          .content
           {{ job.errorMsg }}
</template>

<script>

import Dselect from '@/components/jobinfo/Dselect';
import JobTable from '@/components/jobinfo/JobTable';
import AwsLogs from '@/components/aws/AwsLogs';
import AwsQueue from '@/components/aws/AwsQueue';

export default {
  components: { Dselect, JobTable, AwsLogs, AwsQueue },
  data() {
    return {
      job: {
        sent: false,
        id: '',
        error: false,
        errorMsg: '',
      },
      frm: {
        ref: '',
        r1: '',
        r2: '',
        comments: '',
        disabled: false,
      },
      steps: [
        { name: 'bwa mem', desc: 'bwa mem Ref R1 R2' },
        { name: 'A binario', desc: 'samtools view' },
        { name: 'Ordenar Lecturas', desc: '' },
        { name: 'Marcar Duplicados', desc: '' },
        { name: 'Indice del BAM', desc: '' },
        { name: 'Recalibramiento', desc: '' },
        { name: 'Aplicar Recalibr.', desc: '' },
        { name: 'Variantes Germinales', desc: '', last: true },
      ],
      ref: [
        { name: 'UCSC.HG19', value: 'ucsc.hg19.fasta' },
      ],
      r1: [
        { name: 'A89-A7-L8-1', value: 'A89_USPD16084927-A7_HJ2Y2BBXX_L8_1.fq.gz' },
      ],
      r2: [
        { name: 'A89-A7-L8-2', value: 'A89_USPD16084927-A7_HJ2Y2BBXX_L8_2.fq.gz' },
      ],
    };
  },
  created() {
    this.$on('bwa-queue-error', (error) => {
      this.job.sent = false;
      this.job.error = true;
      this.job.errorMsg = error.message;
    });
    this.$on('bwa-queue-success', (data) => {
      this.job.sent = true;
    });
  },
  methods: {
    sendJob() {
      if (this.frm.disabled) {
        return;
      }
      const jobInfo = {
        jobid: '',
        submitted: '',
        user: '',
        ref: this.frm.ref,
        r1: this.frm.r1,
        r2: this.frm.r2,
        comments: this.frm.comments,
      };
      this.job.id = this.uuidv4();
      jobInfo.jobid = this.job.id;
      this.frm.disabled = true;
      this.$emit('send-to-bwa-queue', jobInfo);
    },
      
    // https://stackoverflow.com/questions/47319320/how-to-generate-unique-persistent-url-which-redirects-to-random-user-defined-arr
    uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxx'.replace(/[xy]/g, (c) => {
        // eslint-disable-next-line
        const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    },
  },
};
</script>

<style  scoped>
.fonta{
  color: #056571;
}

</style>
